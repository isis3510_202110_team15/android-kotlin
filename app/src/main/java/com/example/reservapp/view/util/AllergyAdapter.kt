package com.example.reservapp.view.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.recyclerview.widget.RecyclerView
import com.example.reservapp.R
import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.view.activity.ProfileActivity
import kotlinx.android.synthetic.main.allergy_card.view.*

class AllergyAdapter(private val allergiesList: List<AllergyDTO>,private val profileAllergies: List<AllergyDTO>, private val activity: ProfileActivity): RecyclerView.Adapter<AllergyAdapter.AllergyViewHolder>() {

    inner class AllergyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val checkBox : CheckBox = itemView.checkBox

        init
        {
            checkBox.setOnCheckedChangeListener { _: CompoundButton, b: Boolean ->
                activity.checkboxClick(allergiesList[adapterPosition], b)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllergyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.allergy_card,
            parent, false
        )
        return AllergyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: AllergyViewHolder, position: Int) {
        val currentItem = allergiesList[position]

        holder.checkBox.text = currentItem.name

        for (item in profileAllergies){
            if(item.name == currentItem.name){
                holder.checkBox.isChecked = true
            }
        }
    }

    override fun getItemCount() = allergiesList.size

}