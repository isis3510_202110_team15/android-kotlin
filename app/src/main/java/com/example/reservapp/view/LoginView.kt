package com.example.reservapp.view

interface LoginView {

    fun login()

    fun setUp()

    fun goToHome(checked:Boolean)

    fun showError(message:String)

    fun translateError(error:String):String
}