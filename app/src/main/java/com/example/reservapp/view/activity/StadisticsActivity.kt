package com.example.reservapp.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.reservapp.R
import com.example.reservapp.databinding.ActivityEstadisticsBinding
import com.example.reservapp.model.dto.BookingDTO
import com.example.reservapp.model.dto.RestaurantDTO
import com.example.reservapp.presenter.StadisticsPresenterImp
import com.example.reservapp.view.util.ConnectivityManagement
import com.example.reservapp.view.util.StadisticsView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_estadistics.offline_bar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class StadisticsActivity : AppCompatActivity(), StadisticsView, ConnectivityManagement {

    private lateinit var stadisticsPresenter : StadisticsPresenterImp

    private val stadisticsActivity  = this
    private lateinit var dataBinding: ActivityEstadisticsBinding
    private val job = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + job)

    init{
        coroutineScope.launch {
            dataBinding = DataBindingUtil.setContentView(stadisticsActivity, R.layout.activity_estadistics)
            stadisticsPresenter = StadisticsPresenterImp(stadisticsActivity)
            val userEmail = stadisticsActivity.getSharedPreferences("data", Context.MODE_PRIVATE).getString("email", "")!!
            stadisticsPresenter.getBookingsbyUser(userEmail)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_estadistics)
        dataBinding = DataBindingUtil.setContentView(stadisticsActivity, R.layout.activity_estadistics)

    }

    @SuppressLint("SetTextI18n")
    override fun drawStadistics(list: MutableList<BookingDTO>) {
        if(!isOnline()){
            this.offline_bar.visibility = View.VISIBLE
        }
        if(list.isEmpty()){
            dataBinding.restaurantText.text = "No data"
            dataBinding.categoryText.text = "No data"
            dataBinding.customerText.text = "No data"
            dataBinding.timeText.text = "No data"
            dataBinding.neighborhoodText.text = "No data"
        }
        else{
            var partialMaxCategory = 1
            var definitiveMaxCategory = 0
            var favRestaurantCategoryName = ""
            var partialMax = 1
            var definitiveMax = 0
            var favRestaurantName  = ""
            var favCategoryName  = ""
            var sumPeopleQuantity = 0
            var peopleQuantityMedia = 0
            var sumHours = 0
            var hoursMedia = 0
            var i = 0
            var date = ""
            var splitBlank: List<String>
            var splitComma: List<String>
            val hour: Int
            var partialName: String
            var partialCategory: String
            list.sortBy { it.restaurantName }

            if(list.size == 1){
                favCategoryName = list[0].restaurantCategory
                favRestaurantName = list[0].restaurantName
                date = list[0].date
                splitBlank = date.split(" ")
                splitComma = date.split(",")

                if(splitComma.size > 1){
                    hour = splitComma[0].split(":")[0].replace("\\s".toRegex(), "").toInt()
                    sumHours += hour
                }else{
                    hour = splitBlank[1].split(":")[0].toInt()
                    sumHours += hour
                }
                sumPeopleQuantity = list[0].peopleQuantity

            }
            while (i<list.size-1){

                date = list[i].date
                splitBlank = date.split(" ")
                splitComma = date.split(",")
                var hour: Int

                if(splitComma.size > 1){
                    hour = splitComma[0].split(":")[0].replace("\\s".toRegex(), "").toInt()
                    sumHours += hour
                }else{
                    hour = splitBlank[1].split(":")[0].toInt()
                    sumHours += hour
                }

                if(i == list.size-2){
                    val date2 = list[i+1].date
                    val splitBlank2 = date2.split(" ")
                    val splitComma2 = date2.split(",")
                    var hour2: Int

                    if(splitComma2.size > 1){
                        hour2 = splitComma2[0].split(":")[0].replace("\\s".toRegex(), "").toInt()
                        sumHours += hour2
                    }else{
                        hour2 = splitBlank2[1].split(":")[0].toInt()
                        sumHours += hour2
                    }
                }

                sumPeopleQuantity += list[i].peopleQuantity
                partialName = list[i].restaurantName
                partialCategory = list[i].restaurantCategory
                if(partialName == list[i+1].restaurantName) {
                    partialMax++
                    i++
                    if(partialMax > definitiveMax){
                        favRestaurantName = partialName
                        favRestaurantCategoryName = partialCategory
                        definitiveMax = partialMax
                    }
                }
                else{
                    if(partialMax > definitiveMax){
                        favRestaurantName = partialName
                        favRestaurantCategoryName = partialCategory
                        definitiveMax = partialMax
                        partialMax = 1
                        i++
                    }
                    else{
                        partialMax = 1
                        i++
                    }
                }
            }
            sumPeopleQuantity += list.last().peopleQuantity

            list.sortBy { it.restaurantCategory }
            i = 0
            while (i<list.size-1){
                partialCategory = list[i].restaurantCategory
                if(partialCategory == list[i+1].restaurantCategory) {
                    partialMaxCategory++
                    i++
                    if(partialMaxCategory > definitiveMaxCategory){
                        favCategoryName = partialCategory
                        definitiveMaxCategory = partialMaxCategory
                    }
                }
                else{
                    if(partialMaxCategory > definitiveMaxCategory){
                        favCategoryName = partialCategory
                        definitiveMaxCategory = partialMaxCategory
                        partialMaxCategory = 1
                        i++
                    }
                    else{
                        partialMaxCategory = 1
                        i++
                    }
                }
            }
            coroutineScope.launch {
                stadisticsPresenter.getResturantByNameAndCategory(favRestaurantName, favRestaurantCategoryName)
            }

            if(list.size != 0){
                peopleQuantityMedia = sumPeopleQuantity / list.size
                hoursMedia = sumHours / list.size
            }

            when (hoursMedia) {
                in 6..11 -> {
                    dataBinding.timeText.text = "Morning"
                }
                in 12..17 -> {
                    dataBinding.timeText.text = "Afternoon"
                }
                else -> {
                    dataBinding.timeText.text = "Evening"
                }
            }

            when {
                peopleQuantityMedia <= 1 -> {
                    dataBinding.customerText.text = "Lonewolf"
                }
                peopleQuantityMedia <= 2 -> {
                    dataBinding.customerText.text = "Lover"
                }
                else -> {
                    dataBinding.customerText.text = "Socialite"
                }
            }


            dataBinding.restaurantText.text = favRestaurantName
            dataBinding.categoryText.text = favCategoryName
        }

    }

    override fun drawNeighborhood(restaurant: RestaurantDTO) {
        dataBinding.neighborhoodText.text = restaurant.neighborhood
    }

    @SuppressLint("ShowToast")
    override fun showError(message: String) {
        runOnUiThread {
            Snackbar.make(dataBinding.root, message, Snackbar.LENGTH_LONG).setDuration(3500).show()
        }
    }

    override fun isOnline(): Boolean {
        val connectivityMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        var isConnected = false
        val allNetworks: Array<Network> =
            connectivityMgr.allNetworks // added in API 21 (Lollipop)
        for (network in allNetworks) {
            val networkCapabilities: NetworkCapabilities? =
                connectivityMgr.getNetworkCapabilities(network)
            if (networkCapabilities != null) {
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                ) isConnected = true
            }
        }
        return isConnected
    }
}