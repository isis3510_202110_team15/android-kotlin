package com.example.reservapp.view

import android.widget.Button
import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.CategoryDTO
import com.example.reservapp.model.dto.RestaurantDTO

interface HomeView {

    fun clickBurguerButton()

    fun clickOptionBurger()

    fun drawCategories(lista: MutableList<CategoryDTO>?)

    fun setUp(categoriasDTO:List<CategoryDTO>?)

    fun obtainBurguerOption(button: Button)

    fun clickCategoryRestaurant(option: String, name: String)

    fun showError(message:String)

    fun intentQrResult(restaurant: RestaurantDTO)

    fun setUpAllergiesInSharedPreferences(allergiasUsuario:List<AllergyDTO>)

    fun clickQrButton()
    
    fun clickLogout()
}