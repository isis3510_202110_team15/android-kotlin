package com.example.reservapp.view.util

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.reservapp.R

class LoadingDialogFragment: DialogFragment() {
    companion object {
        private const val FRAGMENT_TAG = "busy"

        private fun newInstance() = LoadingDialogFragment()

        fun show(supportFragmentManager: FragmentManager): LoadingDialogFragment {
            val dialog = newInstance()
            dialog.isCancelable = false
            dialog.show(supportFragmentManager, FRAGMENT_TAG)
            return dialog
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return activity!!.layoutInflater.inflate(R.layout.loading_layout, container)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.also { window ->
            window.attributes?.also { attributes ->
                attributes.dimAmount = 0.1f
                window.attributes = attributes
            }
        }
    }
}