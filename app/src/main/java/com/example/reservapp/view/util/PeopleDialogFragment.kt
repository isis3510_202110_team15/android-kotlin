package com.example.reservapp.view.util

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

class PeopleDialogFragment : DialogFragment() {

    private lateinit var listener: PeopleDialogListener

    interface PeopleDialogListener {
        fun onNumberSelected(dialog: DialogFragment, num: String)
    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)

            val nums = Array(12) { i -> "" + (i + 1) }
            builder.setTitle("Book for _ people").setItems(nums
            ) { _, which ->
                listener.onNumberSelected(this, nums[which])
            }

            builder.create()
        }!!
    }

    fun setListener(listener: PeopleDialogListener) {
        this.listener = listener
    }

}