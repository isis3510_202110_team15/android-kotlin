package com.example.reservapp.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.reservapp.R
import com.example.reservapp.databinding.ActivityProfileBinding
import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.UserDTO
import com.example.reservapp.presenter.ProfilePresenterImp
import com.example.reservapp.view.ProfileView
import com.example.reservapp.view.util.AllergyAdapter
import com.example.reservapp.view.util.ConnectivityManagement
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ProfileActivity : AppCompatActivity(), ProfileView, ConnectivityManagement {

    private lateinit var profilePresenter : ProfilePresenterImp
    private val profileActivity = this

    private var selectedAllergies = mutableListOf<AllergyDTO>()

    private lateinit var dataBinding: ActivityProfileBinding

    private val job = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + job)


    init{
        coroutineScope.launch {
            profilePresenter = ProfilePresenterImp(profileActivity)
            recycler_view.layoutManager = LinearLayoutManager(profileActivity)
            val sharedPref = profileActivity.getSharedPreferences("data",Context.MODE_PRIVATE)
            val logged = sharedPref.getString("email", "")
            profilePresenter.obtainUserAndAllergies(logged.toString())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
    }

    override fun addAllergy(allergies:MutableList<AllergyDTO>) {
        val sharedPref = this.getSharedPreferences("data",Context.MODE_PRIVATE) ?: return
        val logged = sharedPref.getString("email", "")
        var allergiesSeparatedByCommas = ""

        for(item in allergies){
            allergiesSeparatedByCommas += if(item != allergies.last()){
                item.name + ","
            } else{
                item.name
            }
        }

        with(sharedPref.edit()) {
            putString("allergies", allergiesSeparatedByCommas)
            commit()
        }
        coroutineScope.launch {
            profilePresenter.putAllergies(logged.toString(), allergies)
        }

    }

    fun checkboxClick(allergy: AllergyDTO, bool: Boolean){
        if(!bool){
            selectedAllergies.remove(allergy)
        }
        else{
            if(!selectedAllergies.contains(allergy)){
                selectedAllergies.add(allergy)
            }

        }
    }

    override fun drawAllergies(allergiesList: MutableList<AllergyDTO>, profileAllergies: MutableList<AllergyDTO> ){
        recycler_view.adapter = AllergyAdapter(allergiesList, profileAllergies, this)
    }

    override fun setUp(user: UserDTO, allergiesList: MutableList<AllergyDTO>, profileAllergies: MutableList<AllergyDTO>) {
        dataBinding.nameContent.text = user.fullName
        dataBinding.phoneContent.text = user.phone
        dataBinding.emailContent.text = user.email

        buttonAcceptChanges.setOnClickListener{
            addAllergy(selectedAllergies)
        }

        if(!isOnline()){
            this.offline_bar.visibility = View.VISIBLE
        }

        drawAllergies(allergiesList, profileAllergies)

    }

    override fun showError(message: String) {
        runOnUiThread {
            Snackbar.make(dataBinding.root, message, Snackbar.LENGTH_LONG).setDuration(3500).show()
        }
    }

    @SuppressLint("ShowToast")
    override fun confirmUpdateAllergies(allergies: String) {
        runOnUiThread {
            Snackbar.make(findViewById(android.R.id.content), allergies, Snackbar.LENGTH_LONG).setDuration(3500).show()
        }
    }

    override fun isOnline(): Boolean {
        val connectivityMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        var isConnected = false
        val allNetworks: Array<Network> =
            connectivityMgr.allNetworks // added in API 21 (Lollipop)
        for (network in allNetworks) {
            val networkCapabilities: NetworkCapabilities? =
                connectivityMgr.getNetworkCapabilities(network)
            if (networkCapabilities != null) {
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                ) isConnected = true
            }
        }
        return isConnected
    }

}
