package com.example.reservapp.view.activity

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.reservapp.R
import com.example.reservapp.model.dto.BookingDTO
import com.example.reservapp.presenter.BookingsPresenter
import com.example.reservapp.view.BookingsView
import com.example.reservapp.view.util.BookingsAdapter
import com.example.reservapp.view.util.BookingsDB
import com.example.reservapp.view.util.ConnectivityManagement
import kotlinx.android.synthetic.main.restaurant_view_layout.*
import kotlinx.coroutines.launch


class BookingsActivity : AppCompatActivity(), BookingsView, ConnectivityManagement {

    private var userEmail = ""
    var bookings = mutableListOf<BookingDTO>()
    lateinit var bookingsPresenter: BookingsPresenter
    val database by lazy { BookingsDB.getDatabase(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bookings)
        userEmail = this.getSharedPreferences("data", Context.MODE_PRIVATE).getString("email", "")!!
        bookingsPresenter = BookingsPresenter(this)
        fillList()
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)
    }

    override fun setUp(bookingsMuta : MutableList<BookingDTO>) {
        bookings = bookingsMuta

        lifecycleScope.launch {
            if(!isOnline()){
                val localBookings = database.bookingDao().getAll()
                drawBookings(localBookings)
            } else {
                database.bookingDao().deleteAll()

                for(i in bookings){
                    database.bookingDao().insertBooking(i)
                }

            }
        }
    }

    private fun fillList(){
        bookingsPresenter.getBookingsbyUser(userEmail)
    }

    override fun isOnline(): Boolean {
        val connectivityMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        var isConnected = false
        val allNetworks: Array<Network> =
            connectivityMgr.allNetworks // added in API 21 (Lollipop)
        for (network in allNetworks) {
            val networkCapabilities: NetworkCapabilities? =
                connectivityMgr.getNetworkCapabilities(network)
            if (networkCapabilities != null) {
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                ) isConnected = true
            }
        }
        return isConnected
    }

    override fun getBookings(bookingsList: MutableList<BookingDTO>){
        bookings = bookingsList
    }

    override fun drawBookings(bookingsList: MutableList<BookingDTO>) {
        recycler_view.adapter = BookingsAdapter(bookingsList)
        if(!isOnline()){
            this.offline_bar.visibility = View.VISIBLE
        }
    }
}