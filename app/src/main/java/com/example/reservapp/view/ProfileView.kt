package com.example.reservapp.view

import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.UserDTO

interface ProfileView {

    fun addAllergy(allergies: MutableList<AllergyDTO>)

    fun setUp(user: UserDTO, allergiesList: MutableList<AllergyDTO>, profileAllergies: MutableList<AllergyDTO>)

    fun showError(message: String)

    fun confirmUpdateAllergies(allergies: String)

    fun drawAllergies(allergiesList: MutableList<AllergyDTO>, profileAllergies: MutableList<AllergyDTO> )

}