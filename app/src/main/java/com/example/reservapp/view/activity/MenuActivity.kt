package com.example.reservapp.view.activity

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.reservapp.R
import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.MenuDTO
import com.example.reservapp.presenter.MenuPresenter
import com.example.reservapp.view.MenuView
import com.example.reservapp.view.util.ConnectivityManagement
import com.example.reservapp.view.util.MenuAdapter
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity(), MenuView, ConnectivityManagement {

    private var selectedRestaurant = ""
    var menu = MenuDTO()
    private lateinit var menuPresenter: MenuPresenter
    private var userAllergies = mutableListOf<AllergyDTO>()
    private var userEmail = ""

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        selectedRestaurant = intent.getStringExtra("IDRestaurant")
        userEmail = this.getSharedPreferences("data", Context.MODE_PRIVATE).getString("email", "")!!
        menuPresenter = MenuPresenter(this)
        getUserAllergies(userEmail)
        entrees_list.layoutManager = LinearLayoutManager(this)
        entrees_list.setHasFixedSize(true)
        mains_list.layoutManager = LinearLayoutManager(this)
        mains_list.setHasFixedSize(true)
        sides_list.layoutManager = LinearLayoutManager(this)
        sides_list.setHasFixedSize(true)
        drinks_list.layoutManager = LinearLayoutManager(this)
        drinks_list.setHasFixedSize(true)
        desserts_list.layoutManager = LinearLayoutManager(this)
        desserts_list.setHasFixedSize(true)
    }

    override fun setUp(allergies: MutableList<AllergyDTO>) {
        userAllergies = allergies
        menuPresenter.getMenuByRestaurantId(selectedRestaurant)
        drawMenu(menu)
    }

    override fun drawMenu(menuDTO: MenuDTO?) {

        Log.e("UserAllergies", userAllergies.toString())

        if(menuDTO?.entrees != null) {
            entrees_list.adapter = MenuAdapter(menuDTO.entrees!!, userAllergies)
        }

        if(menuDTO?.mains != null) {
            mains_list.adapter = MenuAdapter(menuDTO.mains!!, userAllergies)
        }

        if(menuDTO?.sides != null) {
            sides_list.adapter = MenuAdapter(menuDTO.sides!!, userAllergies)
        }

        if(menuDTO?.drinks != null) {
            drinks_list.adapter = MenuAdapter(menuDTO.drinks!!, userAllergies)
        }

        if(menuDTO?.desserts != null) {
            desserts_list.adapter = MenuAdapter(menuDTO.desserts!!, userAllergies)
        }

        if(!isOnline()) {
            this.offline_bar_menu.visibility = View.VISIBLE
        }
    }

    override fun getUserAllergies(user: String) {
        menuPresenter.getUserAllergies(user)
    }


    override fun setAllergies(profileAllergies: MutableList<AllergyDTO>){
        userAllergies = profileAllergies
    }

    override fun hideLoadingDialog() {

    }

    override fun isOnline(): Boolean {
        val connectivityMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        var isConnected = false
        val allNetworks: Array<Network> =
            connectivityMgr.allNetworks // added in API 21 (Lollipop)
        for (network in allNetworks) {
            val networkCapabilities: NetworkCapabilities? =
                connectivityMgr.getNetworkCapabilities(network)
            if (networkCapabilities != null) {
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                ) isConnected = true
            }
        }
        return isConnected
    }
}