package com.example.reservapp.view.util

import android.app.Dialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.app.TimePickerDialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import com.example.reservapp.R
import java.util.*


class TimePickerFragment : DialogFragment() {
    private lateinit var listener: OnTimeSetListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c: Calendar = Calendar.getInstance()
        val hourOfDay: Int = c.get(Calendar.HOUR_OF_DAY)
        val minute: Int = c.get(Calendar.MINUTE)

        return TimePickerDialog(requireActivity(), R.style.DialogTheme, listener, hourOfDay, minute, false)
    }

    override fun onStart() {
        super.onStart()
        (dialog as TimePickerDialog).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ResourcesCompat.getColor(resources, R.color.color_primary, null))
        (dialog as TimePickerDialog).getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(resources, R.color.color_primary, null))

    }

    fun setListener(listener: OnTimeSetListener) {
        this.listener = listener
    }
}