package com.example.reservapp.view.util

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.reservapp.R
import com.example.reservapp.model.dto.BookingDTO
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.booking_card.view.*
import kotlinx.android.synthetic.main.restaurant_card.view.restaurant_address
import kotlinx.android.synthetic.main.restaurant_card.view.restaurant_name

class BookingsAdapter(
    private val bookingsList: List<BookingDTO>
) : RecyclerView.Adapter<BookingsAdapter.BookingViewHolder>() {

    class BookingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val restaurantName: TextView = itemView.restaurant_name
        val bookingDate: TextView = itemView.booking_date
        val restaurantAddress: TextView = itemView.restaurant_address
        val image: ImageView = itemView.restaurant_image
        val peopleQuantity: TextView = itemView.people_quantity

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookingViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.booking_card,
            parent, false)
        return BookingViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: BookingViewHolder, position: Int) {
        val currentItem = bookingsList[position]
        holder.restaurantName.text = currentItem.restaurantName
        holder.bookingDate.text = currentItem.date
        holder.restaurantAddress.text = currentItem.restaurantAddress
        if(currentItem.peopleQuantity == 1){
            holder.peopleQuantity.text = "For " + currentItem.peopleQuantity.toString() + " Person"
        } else {
            holder.peopleQuantity.text = "For " + currentItem.peopleQuantity.toString() + " People"
        }
        Picasso.get().load(currentItem.image).placeholder(R.drawable.dish).into(
            holder.image
        )

    }

    override fun getItemCount(): Int = bookingsList.size
}