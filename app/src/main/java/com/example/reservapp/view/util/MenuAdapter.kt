package com.example.reservapp.view.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.reservapp.R
import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.DishDTO
import kotlinx.android.synthetic.main.dish_card.view.*

class MenuAdapter(private val dishesList: List<DishDTO>, private val userAllergies: List<AllergyDTO>) :RecyclerView.Adapter<MenuAdapter.MenuViewHolder>() {

    class MenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dishName: TextView = itemView.dish_name
        var dishDescription : TextView = itemView.dish_description
        var dishPrice : TextView = itemView.dish_price
        var allergy : View? = itemView.allergy
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.dish_card,
            parent, false)
        return MenuViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        val currentItem = dishesList[position]

        if(currentItem.ingredients != null)
        for(i in userAllergies){
            for(j in currentItem.ingredients){
                if(i.name == j){
                    holder.allergy?.visibility = View.VISIBLE
                }
            }
        }

        holder.dishName.text = currentItem.name
        holder.dishDescription.text = currentItem.description
        holder.dishPrice.text = String.format("%.2f", currentItem.price)
    }

    override fun getItemCount(): Int = dishesList.size
}
