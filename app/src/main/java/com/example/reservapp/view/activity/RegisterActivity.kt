package com.example.reservapp.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import com.example.reservapp.R
import com.example.reservapp.databinding.ActivityRegisterBinding
import com.example.reservapp.presenter.RegisterPresenter
import com.example.reservapp.view.RegisterView
import com.example.reservapp.view.util.ConnectivityManagement
import com.example.reservapp.view.util.FirebaseActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_register.*
import java.util.regex.Pattern

class RegisterActivity : FirebaseActivity(), RegisterView, ConnectivityManagement {

    private lateinit var registerPresenter: RegisterPresenter
    lateinit var binding : ActivityRegisterBinding

    var selectedIdType = ""

    lateinit var idTypes: Array<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        setUp()
    }

    override fun register() {
        val email = emailEditTextReg.text.toString()
        val password = passEditTextReg.text.toString()
        val id = idTextView.text.toString()
        val phone = phoneEditText.text.toString()
        val name = fullNameEditText.text.toString()
        val validEmail = email.isValidEmail()
        val validPass = password.isValidPassword()
        val validId = id.isValidNameOrID()
        val validPhone = phone.isValidPhone()
        val validName = name.isValidNameOrID()
        val validRePass = rePassEditText.text.toString() == passEditTextReg.text.toString()
        if (validEmail && validId && validName && validPass && validPhone && validRePass) {
            callFirebase { registerPresenter.register(
                    email,
                    password,
                    selectedIdType,
                    id,
                    name,
                    phone
                )
            }
        }
    }

    override fun setUp() {
        registerPresenter = RegisterPresenter(this)
        val actionBar = supportActionBar
        actionBar?.title = getString(R.string.registerAppBarTitle)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        idTextView.validate(getString(R.string.idInputError)) { s -> s.isValidNameOrID() }
        fullNameEditText.validate(getString(R.string.nameInputError)) { s -> s.isValidNameOrID() }
        emailEditTextReg.validate(getString(R.string.emailInputError)) { s -> s.isValidEmail() }
        passEditTextReg.validate(getString(R.string.passInputError)) { s -> s.isValidPassword() }
        rePassEditText.validate(getString(R.string.differentPasswordsError)) { s -> s == passEditTextReg.text.toString() }
        phoneEditText.validate(getString(R.string.phoneInputError)) { s -> s.isValidPhone() }
        registerBtn.setOnClickListener { register() }
        setUpSpinner()
    }

    @SuppressLint("ShowToast")
    override fun showError(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).setDuration(3500).show()
    }

    override fun translateError(error: String): String {
        return getString(resources.getIdentifier(error, "string", packageName))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun goToLogin() {
        val intent = Intent(applicationContext, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun setUpSpinner() {
        idTypes = resources.getStringArray(R.array.idTypes)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, idTypes)
        idTypeSpinner.adapter = adapter
        idTypeSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                selectedIdType = idTypes[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }
    }

    private fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                afterTextChanged.invoke(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    private fun EditText.validate(message: String, validator: (String) -> Boolean) {
        this.afterTextChanged {
            this.error = if (validator(it)) null else message
        }
    }

    private fun String.isValidEmail(): Boolean {
        return this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    private fun String.isValidNameOrID(): Boolean {
        return this.length > 6
    }

    private fun String.isValidPhone(): Boolean {
        return this.length == 10
    }

    private fun String.isValidPassword(): Boolean {
        val regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%!\\-_?&])(?=\\S+\$).{8,}"

        val pattern = Pattern.compile(regex)
        val matcher = pattern.matcher(this)

        return matcher.matches()
    }

    override fun isOnline(): Boolean {
        val connectivityMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        var isConnected = false
        val allNetworks: Array<Network> =
            connectivityMgr.allNetworks // added in API 21 (Lollipop)
        for (network in allNetworks) {
            val networkCapabilities: NetworkCapabilities? =
                connectivityMgr.getNetworkCapabilities(network)
            if (networkCapabilities != null) {
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                ) isConnected = true
            }
        }
        return isConnected
    }

}