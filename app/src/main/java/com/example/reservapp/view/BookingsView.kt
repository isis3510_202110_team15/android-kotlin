package com.example.reservapp.view

import com.example.reservapp.model.dto.BookingDTO

interface BookingsView {

    fun drawBookings(bookingsList: MutableList<BookingDTO>)

    fun getBookings(bookingsList: MutableList<BookingDTO>)

    fun setUp(bookingsMuta: MutableList<BookingDTO>)
}