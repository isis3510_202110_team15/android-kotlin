package com.example.reservapp.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import com.example.reservapp.R
import com.example.reservapp.databinding.ActivityLoginBinding
import com.example.reservapp.presenter.LoginPresenter
import com.example.reservapp.view.LoginView
import com.example.reservapp.view.util.ConnectivityManagement
import com.example.reservapp.view.util.FirebaseActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import java.util.regex.Pattern


class LoginActivity : FirebaseActivity(), LoginView, ConnectivityManagement {

    private lateinit var loginPresenter: LoginPresenter
    lateinit var binding : ActivityLoginBinding

    var email = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        goToHome(false)
    }

    override fun login() {
        email = emailEditText.text.toString()
        val password = passEditText.text.toString()
        if (email.isValidEmail() && password.isValidPassword()) {
            callFirebase { loginPresenter.login(email, password) }
        }
    }

    override fun setUp() {
        loginPresenter = LoginPresenter(this)
        logInBtn.setOnClickListener { login() }
        registerTextView.setOnClickListener { goToRegister() }
        if(!isOnline()){
            this.offline_bar.visibility = View.VISIBLE
        }
        emailEditText.validate("Valid email address required") { s -> s.isValidEmail() }
        passEditText.validate("Use 1 cap letter, 1 number abd 1 symbol") { s -> s.isValidPassword() }
    }

    @SuppressLint("ShowToast")
    override fun showError(message: String) {
        runOnUiThread {
            Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).setDuration(3500).show()
        }
    }

    override fun translateError(error: String): String {
        return getString(resources.getIdentifier(error, "string", packageName))
    }

    private fun goToRegister() {
        val intent = Intent(applicationContext, RegisterActivity::class.java)
        startActivity(intent)
    }

    override fun goToHome(checked: Boolean) {

        val sharedPref = this.getSharedPreferences("data", Context.MODE_PRIVATE) ?: return
        val logged = sharedPref.getString("email", "")
        if (!logged.isNullOrEmpty() || checked) {
            if (checked) {
                with(sharedPref.edit()) {
                    putString("email", email)
                    commit()
                }
            }
            val intent = Intent(applicationContext, HomeActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            setUp()
        }
    }

    private fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                afterTextChanged.invoke(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    private fun EditText.validate(message: String, validator: (String) -> Boolean) {
        this.afterTextChanged {
            this.error = if (validator(it)) null else message
        }
    }

    private fun String.isValidEmail(): Boolean {
        return this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    private fun String.isValidPassword(): Boolean {
        val regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%!\\-_?&])(?=\\S+\$).{8,}"

        val pattern = Pattern.compile(regex)
        val matcher = pattern.matcher(this)

        return matcher.matches()
    }

    override fun isOnline(): Boolean {
        val connectivityMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        var isConnected = false
        val allNetworks: Array<Network> =
            connectivityMgr.allNetworks // added in API 21 (Lollipop)
        for (network in allNetworks) {
            val networkCapabilities: NetworkCapabilities? =
                connectivityMgr.getNetworkCapabilities(network)
            if (networkCapabilities != null) {
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                ) isConnected = true
            }
        }
        return isConnected
    }

    override fun onBackPressed() {
        //login do not allow back pressed
    }

}