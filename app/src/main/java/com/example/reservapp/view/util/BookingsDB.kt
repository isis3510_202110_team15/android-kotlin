package com.example.reservapp.view.util

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.reservapp.model.dao.BookingDAO
import com.example.reservapp.model.dto.BookingDTO

@Database(entities = [BookingDTO::class], version = 2)
abstract class BookingsDB : RoomDatabase() {
    abstract fun bookingDao(): BookingDAO

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: BookingsDB? = null

        fun getDatabase(context: Context): BookingsDB {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    BookingsDB::class.java,
                    "word_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}