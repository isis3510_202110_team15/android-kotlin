package com.example.reservapp.view

import com.example.reservapp.model.dto.RestaurantDTO

interface RestaurantView {

    fun setUp()

    fun showMessage(message:String)

    fun readCurrentlyUbication()

    fun drawRestaurant(restaurantList: MutableList<RestaurantDTO>)
}