package com.example.reservapp.view.activity


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.reservapp.R
import com.example.reservapp.model.dto.RestaurantDTO
import com.example.reservapp.presenter.RestaurantPresenter
import com.example.reservapp.view.RestaurantView
import com.example.reservapp.view.util.ConnectivityManagement
import com.example.reservapp.view.util.RestaurantAdapter
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.restaurant_view_layout.*
import kotlin.math.acos
import kotlin.math.cos
import kotlin.math.sin

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class RestaurantActivity : AppCompatActivity(), RestaurantView, ConnectivityManagement {

    private var selectedCategory = ""
    private var categoryName = ""
    val restaurantActivity = this
    lateinit var restaurantPresenter: RestaurantPresenter

    lateinit var mFusedLocationClient: FusedLocationProviderClient
    @Suppress("PrivatePropertyName")
    private val PERMISSION_ID = 42

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.restaurant_view_layout)

        if (allPermissionsGrantedGPS()){
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            readCurrentlyUbication()
        } else {
            // Si no hay permisos solicitarlos al usuario.
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_ID)
        }

        selectedCategory = intent.getStringExtra("ID")
        categoryName = intent.getStringExtra("NAME")
        setUp()
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)

    }

    override fun showMessage(message: String) {
        runOnUiThread {
            Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).setDuration(3500).show()
        }
    }

    override fun setUp() {
        restaurantPresenter = RestaurantPresenter(this)
        restaurant_category_title.text = categoryName
        restaurantPresenter.getRestaurants(selectedCategory)
    }

    override fun drawRestaurant(
        restaurantList: MutableList<RestaurantDTO>
    ) {
        val restaurantListOrdered: MutableList<RestaurantDTO> = orderRestaurantByDistnaceKM(restaurantList)

        recycler_view.adapter = RestaurantAdapter(restaurantListOrdered, restaurantPresenter)
        if(!isOnline()){
            this.offline_bar.visibility = View.VISIBLE
        }
    }

    private fun orderRestaurantByDistnaceKM(restaurantList: MutableList<RestaurantDTO>): MutableList<RestaurantDTO>{
        val sharedPref = this.getSharedPreferences("data",Context.MODE_PRIVATE)
        val latitude = sharedPref.getString("latitude", "0.0").toString().toDouble()
        val longitude = sharedPref.getString("longitude", "0.0").toString().toDouble()
        Log.i("ORDER_REST1ORIGINAL","-------------------------->Restaurants ${restaurantList}<-----------------------------")
        if(latitude == 0.0 || longitude == 0.0){
            return restaurantList
        }
        else{
            Log.i("ORDER_REST0", "HIJOLE SI ME EJECUTE")
            val copy:MutableList<RestaurantDTO> = restaurantList
            for(item in copy){
                item.distance = distanceInKm(latitude, longitude, item.latitude, item.longitude)
            }

            Log.i("ORDER_REST1","-------------------------->Restaurants ${copy}<-----------------------------")

            copy.sortBy { it.distance }

            Log.i("ORDER_REST2","-------------------------->Restaurants ${copy}<-----------------------------")

            return copy

        }
    }

    private fun distanceInKm(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        if(lat2 == 0.0 || lon2 == 0.0){
            val number3digits:Double = String.format("%.3f", Double.MAX_VALUE).toDouble()
            val number2digits:Double = String.format("%.2f", number3digits).toDouble()
            return String.format("%.1f", number2digits).toDouble()
        }
        else{
            val theta = lon1 - lon2
            var dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta))
            dist = acos(dist)
            dist = rad2deg(dist)
            dist *= 60 * 1.1515
            dist *= 1.609344

            val number3digits:Double = String.format("%.3f", dist).toDouble()
            val number2digits:Double = String.format("%.2f", number3digits).toDouble()
            return String.format("%.1f", number2digits).toDouble()
        }

    }

    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }

    override fun readCurrentlyUbication(){
        if (checkPermissions()){
            Log.i("PASO1", "-----------------------")
            if (isLocationEnabled()){
                Log.i("PASO2", "-----------------------")
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    mFusedLocationClient.lastLocation.addOnCompleteListener(this){ task ->
                        val location: Location? = task.result
                        Log.i("PASO3", "-----------------------")
                        if (location == null){
                            requestNewLocationData()
                        } else {
                            Log.i("PASO4", "-----------------------")
                            val sharedPref = this.getSharedPreferences("data", Context.MODE_PRIVATE)

                            with(sharedPref.edit()) {
                                Log.i("PASO5-LATITUD", location.latitude.toString())
                                Log.i("PASO5-LONGITUD", location.longitude.toString())
                                putString("latitude", location.latitude.toString())
                                putString("longitude", location.longitude.toString())
                                commit()
                            }
                        }
                    }
                }
            } else {
                Toast.makeText(this, "Activar ubicación", Toast.LENGTH_SHORT).show()
            }
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_ID)
        }
    }


    private fun allPermissionsGrantedGPS() = REQUIRED_PERMISSIONS_GPS.all {
        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData(){
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallBack, Looper.myLooper())
    }

    private val mLocationCallBack = object : LocationCallback(){
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation : Location = locationResult.lastLocation
            val sharedPref = restaurantActivity.getSharedPreferences("data", Context.MODE_PRIVATE) ?: return

            with(sharedPref.edit()) {
                putString("latitude", mLastLocation.latitude.toString())
                putString("longitude", mLastLocation.longitude.toString())
                commit()
            }
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    companion object {
        private val REQUIRED_PERMISSIONS_GPS= arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    }

    override fun isOnline(): Boolean {
        val connectivityMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        var isConnected = false
        val allNetworks: Array<Network> =
            connectivityMgr.allNetworks // added in API 21 (Lollipop)
        for (network in allNetworks) {
            val networkCapabilities: NetworkCapabilities =
                connectivityMgr.getNetworkCapabilities(network)
            if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
            ) isConnected = true
        }
        return isConnected
    }

}