package com.example.reservapp.view

interface RegisterView {

    fun register()

    fun setUp()

    fun showError(message:String)

    fun translateError(error:String):String

    fun goToLogin()
}