package com.example.reservapp.view

import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.MenuDTO
import com.example.reservapp.model.dto.RestaurantDTO

interface MenuView {
    fun setUp(allergies: MutableList<AllergyDTO>)

    fun drawMenu(menuDTO: MenuDTO?)

    fun getUserAllergies(user: String)

    fun hideLoadingDialog()

    fun setAllergies(profileAllergies: MutableList<AllergyDTO>)
}