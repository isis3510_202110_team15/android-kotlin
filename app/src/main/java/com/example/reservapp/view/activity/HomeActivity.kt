package com.example.reservapp.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.reservapp.R
import com.example.reservapp.databinding.ActivityHomeBinding
import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.CategoryDTO
import com.example.reservapp.model.dto.RestaurantDTO
import com.example.reservapp.presenter.HomePresenterImp
import com.example.reservapp.view.HomeView
import com.example.reservapp.view.util.CategoryAdapter
import com.example.reservapp.view.util.ConnectivityManagement
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.offline_bar
import kotlinx.android.synthetic.main.activity_home.recycler_view
import kotlinx.coroutines.*

@Suppress("PrivatePropertyName")
class HomeActivity : AppCompatActivity(), HomeView, ConnectivityManagement {

    private val TIME_INTERNAL = 1000
    private var categorias: MutableList<CategoryDTO>? = mutableListOf()
    private var backPressed:Long? = null
    private var optionBurger:String? = null
    private var thisHomeActivity= this

    private lateinit var homePresenter : HomePresenterImp
    lateinit var binding : ActivityHomeBinding

    private val job = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + job)

    init{
        coroutineScope.launch {
            homePresenter = HomePresenterImp(thisHomeActivity)
            recycler_view.layoutManager = LinearLayoutManager(thisHomeActivity)
            homePresenter.getCategories()
            val sharedPref = thisHomeActivity.getSharedPreferences("data", MODE_PRIVATE)
            val logged = sharedPref.getString("email", "")
            homePresenter.getAllergiesUser(logged.toString())
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        buttonHamburguerHome.setOnClickListener{
            clickBurguerButton()
        }

        buttonBackBurguer.setOnClickListener{
            noShowBurguerOption()
        }

        buttonProfile.setOnClickListener{
            obtainBurguerOption(buttonProfile)
            clickOptionBurger()
        }

        buttonBookings.setOnClickListener{
            obtainBurguerOption(buttonBookings)
            clickOptionBurger()
        }

        buttonStadistics.setOnClickListener{
            obtainBurguerOption(buttonStadistics)
            clickOptionBurger()
        }

        logOutBtn.setOnClickListener{
            obtainBurguerOption(logOutBtn)
            clickLogout()
        }

        qr_button.setOnClickListener{
            clickQrButton()
        }

    }

    override fun onBackPressed() {
        when {
            backPressed == null -> {
                backPressed = System.currentTimeMillis()
            }
            backPressed!! + TIME_INTERNAL > System.currentTimeMillis() -> {
                super.onBackPressed()
            }
            else -> {
                backPressed = System.currentTimeMillis()
            }
        }
    }

    override fun clickBurguerButton() {

        buguerOptions.visibility = View.VISIBLE
    }

    private fun noShowBurguerOption(){
        buguerOptions.visibility = View.INVISIBLE
    }

    override fun obtainBurguerOption(button: Button){
        optionBurger = button.text.toString()
    }

    override fun clickOptionBurger() {

        when {
            optionBurger!!.compareTo(getString(R.string.profile)) == 0 -> {
                val intent = Intent(this, ProfileActivity::class.java)
                optionBurger = null
                startActivity(intent)
            }
            optionBurger!!.compareTo(getString(R.string.bookings)) == 0 -> {
                val intent = Intent(this, BookingsActivity::class.java)
                optionBurger = null
                startActivity(intent)
            }
            optionBurger!!.compareTo("Stats") == 0 -> {
                val intent = Intent(this, StadisticsActivity::class.java)
                optionBurger = null
                startActivity(intent)
            }
            else -> {
                noShowBurguerOption()
            }
        }
    }

    override fun drawCategories(lista: MutableList<CategoryDTO>?) {
        if(lista != null){
            if(lista.size > 0){
                recycler_view.adapter = CategoryAdapter(lista)
            }
        }

        if(!isOnline()){
            this.offline_bar.visibility = View.VISIBLE
        }
    }

    override fun setUp(categoriasDTO:List<CategoryDTO>? ) {
        categorias = categoriasDTO as MutableList<CategoryDTO>
        drawCategories(categorias)

    }

    override fun setUpAllergiesInSharedPreferences(allergiasUsuario:List<AllergyDTO>){
        val sharedPref = this.getSharedPreferences("data", Context.MODE_PRIVATE) ?: return
        var allergiesSeparatedByCommas = ""
        for(item in allergiasUsuario){
            allergiesSeparatedByCommas += if(item != allergiasUsuario.last()){
                item.name + ","
            } else{
                item.name
            }
        }
        with(sharedPref.edit()) {
            putString("allergies", allergiesSeparatedByCommas)
            commit()
        }


    }

    override fun clickCategoryRestaurant(option: String, name: String) {
        option[2]
        val intent = Intent(applicationContext, RestaurantActivity::class.java)
        intent.putExtra("ID", option).putExtra("NAME", name)
        startActivity(intent)
    }

    @SuppressLint("ShowToast")
    override fun showError(message: String) {
        runOnUiThread {
            Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).setDuration(3500).show()
        }
    }

    override fun clickQrButton() {
        IntentIntegrator(this).initiateScan()
    }

    override fun clickLogout() {
        val sharedPref = this.getSharedPreferences("data",Context.MODE_PRIVATE)
        sharedPref.edit().clear().commit()
        val intent = Intent(applicationContext, LoginActivity::class.java)
        startActivity(intent)


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if(result != null){
            if(result.contents == null){
                Toast.makeText(this, "Cancelado", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "El valor escaneado es: ${result.contents}", Toast.LENGTH_SHORT).show()
                homePresenter.getRestaurantByQR(result.contents)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun intentQrResult(restaurant: RestaurantDTO){
        val intent = Intent(applicationContext, MenuActivity::class.java)
        intent.putExtra("IDRestaurant", restaurant.id )
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
    override fun isOnline(): Boolean {
        val connectivityMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        var isConnected = false
        val allNetworks: Array<Network> =
            connectivityMgr.allNetworks // added in API 21 (Lollipop)
        for (network in allNetworks) {
            val networkCapabilities: NetworkCapabilities? =
                connectivityMgr.getNetworkCapabilities(network)
            if (networkCapabilities != null) {
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                ) isConnected = true
            }
        }
        return isConnected
    }


}
