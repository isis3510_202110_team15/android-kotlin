package com.example.reservapp.view.util

import com.example.reservapp.model.dto.BookingDTO
import com.example.reservapp.model.dto.RestaurantDTO

interface StadisticsView {

    fun drawStadistics(list: MutableList<BookingDTO>)

    fun drawNeighborhood(restaurant: RestaurantDTO)

    fun showError(message:String)

}