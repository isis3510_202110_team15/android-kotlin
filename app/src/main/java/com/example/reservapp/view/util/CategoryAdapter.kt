package com.example.reservapp.view.util

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.reservapp.R
import com.example.reservapp.model.dto.CategoryDTO
import com.example.reservapp.view.activity.RestaurantActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.category_card.view.*

class CategoryAdapter(private val categoryList: List<CategoryDTO>):RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>(){

    var categoryId = ""
    var categoryName = ""

    inner class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
            {
                val imageView: ImageView = itemView.image_view
                val categoryButton : Button = itemView.textButton
                val context: Context = super.itemView.context

                init{
                    categoryButton.setOnClickListener {
                        categoryId = categoryList[adapterPosition].id
                        categoryName = categoryList[adapterPosition].name
                        val intent = Intent(this.context, RestaurantActivity::class.java)
                        intent.putExtra("ID", categoryId).putExtra("NAME", categoryName)
                        startActivity(this.context, intent, null)
                    }

                    imageView.setOnClickListener{
                        categoryId = categoryList[adapterPosition].id
                        categoryName = categoryList[adapterPosition].name
                        val intent = Intent(this.context, RestaurantActivity::class.java)
                        intent.putExtra("ID", categoryId).putExtra("NAME", categoryName)
                        startActivity(this.context, intent, null)
                    }
                }


            }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.category_card,
            parent, false
        )
        return CategoryViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val currentItem = categoryList[position]
        Picasso.get().load(currentItem.image).resize(150,150).centerCrop().placeholder(R.drawable.categorypizza).into(holder.imageView)
        holder.categoryButton.text = currentItem.name
    }

    override fun getItemCount() = categoryList.size

}