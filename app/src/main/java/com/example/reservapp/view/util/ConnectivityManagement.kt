package com.example.reservapp.view.util

interface ConnectivityManagement {
    fun isOnline(): Boolean
}