package com.example.reservapp.view.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity


@Suppress("DEPRECATION")
open class FirebaseActivity: AppCompatActivity() {

    private lateinit var loadingDialog: LoadingDialogFragment


    fun showLoadingDialog() {
        loadingDialog = LoadingDialogFragment.show(supportFragmentManager)
    }

    fun hideLoadingDialog() {
        runOnUiThread {
            loadingDialog.dismiss()
        }
    }

    inline fun callFirebase(func: ()->Unit){
        var result = false
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork
            if(networkCapabilities != null) {
                val actNw =
                    connectivityManager.getNetworkCapabilities(networkCapabilities)
                result = if(actNw != null) {
                    when {
                        actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                        actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                        actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                        else -> false
                    }
                }else{
                    false
                }
            }else{
                result = false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }
        if(result) {
            showLoadingDialog()
            func()
            hideLoadingDialog()
        }
    }
}