package com.example.reservapp.view.util

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import com.example.reservapp.R
import com.example.reservapp.model.dto.RestaurantDTO
import com.example.reservapp.presenter.RestaurantPresenter
import com.example.reservapp.view.activity.MenuActivity
import com.example.reservapp.view.activity.RestaurantActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.restaurant_card.view.*
import java.util.*


class RestaurantAdapter(
    private val restaurantList: List<RestaurantDTO>,
    private val restaurantPresenter: RestaurantPresenter
) :
    RecyclerView.Adapter<RestaurantAdapter.RestaurantViewHolder>() {

    var restaurantId = ""
    var restaurantNameG = ""
    var restaurantAddressG = ""
    var restaurantCategory = ""
    var restaurantImageG = ""
    var selectedDate = ""
    var selectedTime = ""
    var userEmail = ""
    var peopleQuantity = 0

    inner class RestaurantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, PeopleDialogFragment.PeopleDialogListener {
        val imageView: ImageView = itemView.image_view
        val restaurantName: TextView = itemView.restaurant_name
        var restaurantAddress: TextView = itemView.restaurant_address
        var restaurantDistance: TextView = itemView.restaurant_distance
        private val menuButton: ImageButton = itemView.menu_button
        private val calendarButton: ImageButton = itemView.calendar_button

        var context: Context = super.itemView.context

        init {
            userEmail = this.context.getSharedPreferences("data", Context.MODE_PRIVATE).getString(
                "email",
                ""
            )!!

            menuButton.setOnClickListener{
                restaurantId = restaurantList[adapterPosition].id
                val intent = Intent(this.context, MenuActivity::class.java)
                intent.putExtra("IDRestaurant", restaurantId)
                startActivity(this.context, intent, null)
            }

            calendarButton.setOnClickListener {
                restaurantId = restaurantList[adapterPosition].id
                restaurantNameG = restaurantList[adapterPosition].name
                restaurantCategory = restaurantList[adapterPosition].categoryName
                restaurantAddressG = restaurantList[adapterPosition].address
                restaurantImageG = restaurantList[adapterPosition].image.toString()

                val dateFragment = DatePickerFragment()
                dateFragment.setListener(this)
                dateFragment.show(
                    (this.context as RestaurantActivity).supportFragmentManager,
                    "DatePicker"
                )
            }
        }

        override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
            val timeFragment = TimePickerFragment()
            timeFragment.setListener(this)
            timeFragment.show(
                (this.context as RestaurantActivity).supportFragmentManager,
                "TimePicker"
            )

            var dayStr = "$dayOfMonth"
            var monthStr = "$month"

            if(dayOfMonth<10){
                dayStr = "0$dayStr"
            }

            if(month<10){
                monthStr = "0$month"
            }

            selectedDate = "$dayStr/$monthStr/$year"
        }

        @Suppress("DEPRECATED_IDENTITY_EQUALS")
        override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
            var timeOfDay = ""
            val datetime: Calendar = Calendar.getInstance()
            datetime.set(Calendar.HOUR_OF_DAY, hourOfDay)
            datetime.set(Calendar.MINUTE, minute)

            if (datetime.get(Calendar.AM_PM) === Calendar.AM)
                timeOfDay = "AM"
            else if (datetime.get(Calendar.AM_PM) === Calendar.PM)
                timeOfDay = "PM"

            var hourStr = "$hourOfDay"
            var minStr = "$minute"

            if(hourOfDay<10){
                hourStr = "0$hourStr"
            }

            if(minute<10){
                minStr = "0$minStr"
            }

            selectedTime = "$hourStr:$minStr $timeOfDay"

            showPeopleDialog()
        }

        override fun onNumberSelected(dialog: DialogFragment, num: String) {
            peopleQuantity = num.toInt()
            bookTable()
        }

        private fun showPeopleDialog() {
            // Create an instance of the dialog fragment and show it
            val dialog = PeopleDialogFragment()
            dialog.setListener(this)
            dialog.show((this.context as RestaurantActivity).supportFragmentManager, "")
        }
    }




    fun bookTable() {

        val fullDate = "$selectedDate $selectedTime"

        restaurantPresenter.bookTable(
            fullDate,
            restaurantId,
            restaurantNameG,
            restaurantCategory,
            restaurantAddressG,
            restaurantImageG,
            peopleQuantity,
            userEmail
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.restaurant_card,
            parent, false
        )
        return RestaurantViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        val currentItem = restaurantList[position]
        Picasso.get().load(currentItem.image).placeholder(R.drawable.dish).into(
            holder.imageView
        )
        holder.restaurantDistance.text = currentItem.distance.toString() + " Km away."
        holder.restaurantName.text = currentItem.name
        holder.restaurantAddress.text = currentItem.address
    }

    override fun getItemCount() = restaurantList.size
}

