package com.example.reservapp.view.util

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import com.example.reservapp.R
import java.util.*


class DatePickerFragment : DialogFragment() {
    private lateinit var listener: OnDateSetListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c: Calendar = Calendar.getInstance()
        val year: Int = c.get(Calendar.YEAR)
        val month: Int = c.get(Calendar.MONTH)
        val day: Int = c.get(Calendar.DAY_OF_MONTH)

        return DatePickerDialog(requireActivity(), R.style.DialogTheme, listener, year, month, day)
    }

    override fun onStart() {
        super.onStart()
        (dialog as DatePickerDialog).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ResourcesCompat.getColor(resources, R.color.color_primary, null))
        (dialog as DatePickerDialog).getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ResourcesCompat.getColor(resources, R.color.color_primary, null))
        (dialog as DatePickerDialog).datePicker.minDate = System.currentTimeMillis() - 1000
    }

    fun setListener(listener: OnDateSetListener) {
        this.listener = listener
    }
}