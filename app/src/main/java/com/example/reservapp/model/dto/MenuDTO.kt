package com.example.reservapp.model.dto

data class MenuDTO(var entrees: List<DishDTO>? = null, var mains: List<DishDTO>? = null, var sides: List<DishDTO>? = null, var drinks: List<DishDTO>? = null, var desserts: List<DishDTO>? = null)