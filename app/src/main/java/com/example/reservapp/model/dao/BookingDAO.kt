package com.example.reservapp.model.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.reservapp.model.dto.BookingDTO

@Dao
interface BookingDAO {

    @Query("SELECT * FROM BookingDTO")
    suspend fun getAll(): MutableList<BookingDTO>

    @Query("SELECT * FROM BookingDTO WHERE user = :email")
    suspend fun findByUser(email: String): MutableList<BookingDTO>

    @Query("DELETE FROM BookingDTO")
    suspend fun deleteAll()

    @Insert
    suspend fun insertAll(vararg bookings: BookingDTO)

    @Insert
    suspend fun insertBooking(booking: BookingDTO)

    @Delete
    suspend fun delete(booking: BookingDTO)
}