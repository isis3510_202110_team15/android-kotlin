package com.example.reservapp.model.service

import com.example.reservapp.model.dto.BookingDTO
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class BookingsService {

    fun getBookingsByUser(callback: Callback<MutableList<BookingDTO>>, user: String){

        val docRef = Firebase.firestore.collection("bookings").whereEqualTo("user", user)
        docRef.get().addOnSuccessListener { documentSnapshot ->
            val doc = documentSnapshot
            val bookings = doc.toObjects(BookingDTO::class.java)
            for(i in bookings.indices){
                bookings[i].id = doc.documents[i].id
            }
            callback.onSuccess(bookings)
        }
    }
}