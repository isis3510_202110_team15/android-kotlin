package com.example.reservapp.model.service

import android.content.ContentValues
import android.util.Log
import com.example.reservapp.model.dto.BookingDTO
import com.example.reservapp.model.dto.RestaurantDTO
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class StadisticsService {

    fun getBookingsByUser(callback: Callback<MutableList<BookingDTO>>, user: String){

        val docRef = Firebase.firestore.collection("bookings").whereEqualTo("user", user)
        docRef.get().addOnSuccessListener { documentSnapshot ->
            val bookings = documentSnapshot.toObjects(BookingDTO::class.java)
            for (i in bookings.indices) {
                bookings[i].id = documentSnapshot.documents[i].id
            }
            callback.onSuccess(bookings)
        }
    }

    fun getResturantByNameAndCategory( name: String, category: String, callback: Callback<RestaurantDTO>){
        val db = Firebase.firestore

        Log.i("StadisticsService", "Name $name category: $category")

        val collRef = db.collection("restaurants").whereEqualTo("name", name).whereEqualTo("categoryName", category)
        collRef
            .get()
            .addOnSuccessListener { result ->
                val restaurants = result.toObjects(RestaurantDTO::class.java)
                for(i in restaurants.indices){
                    restaurants[i].id = result.documents[i].id
                }
                callback.onSuccess(restaurants[0])
            }
            .addOnFailureListener{ exception ->
                Log.w(ContentValues.TAG, "Error getting documents in StatidisticsService.", exception)
                callback.onFailed(exception)
            }
    }
}