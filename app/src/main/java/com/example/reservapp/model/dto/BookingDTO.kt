package com.example.reservapp.model.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class BookingDTO(
    @PrimaryKey var id: String = "",
    @ColumnInfo(name = "date") var date: String = "",
    @ColumnInfo(name = "restaurantId") var restaurantId: String = "",
    @ColumnInfo(name = "restaurantName") var restaurantName: String = "",
    @ColumnInfo(name = "restaurantCategory") var restaurantCategory: String = "",
    @ColumnInfo(name = "restaurantAddress") var restaurantAddress: String = "",
    @ColumnInfo(name = "image") var image: String = "",
    @ColumnInfo(name = "peopleQuantity") var peopleQuantity: Int = 0,
    @ColumnInfo(name = "user") var user: String = ""
)