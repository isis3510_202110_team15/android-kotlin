    package com.example.reservapp.model.service

    import android.content.ContentValues.TAG
    import android.util.Log
    import com.example.reservapp.model.dto.AllergyDTO
    import com.example.reservapp.model.dto.CategoryDTO
    import com.example.reservapp.model.dto.RestaurantDTO
    import com.example.reservapp.model.dto.UserDTO
    import com.google.firebase.firestore.ktx.firestore
    import com.google.firebase.ktx.Firebase

    class HomeService{

        fun obtainAllergiesUser(email:String,callback: Callback<List<AllergyDTO>>){
            val db = Firebase.firestore
            val collRef = db.collection("users").document(email)
            collRef
                .get()
                .addOnSuccessListener { document ->
                    if (document != null) {

                        val usuario = document.toObject(UserDTO::class.java)
                        val profileAllergies = usuario?.allergies
                        callback.onSuccess(profileAllergies)
                    }
                }.addOnFailureListener{exception ->
                    Log.w(TAG, "Error getting allergies of the user in HomeActivity.", exception)
                    callback.onFailed(exception)

                }
        }

        fun obtainCategoriesRestaurants(callback: Callback<List<CategoryDTO>>) {
            val db = Firebase.firestore

            val collRef = db.collection("categories")
            collRef
                .get()
                .addOnSuccessListener { result ->

                    val categorias = result.toObjects(CategoryDTO::class.java)

                    for(i in categorias.indices){
                        categorias[i].id = result.documents[i].id
                    }

                    callback.onSuccess(categorias)
                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error getting documents.", exception)
                    callback.onFailed(exception)
                }
        }

        fun obtainRestaurantByQR(qr: String,callback: Callback<RestaurantDTO>){
            val db = Firebase.firestore

            val collRef = db.collection("restaurants").whereEqualTo("qrCode", qr)
            collRef
                .get()
                .addOnSuccessListener { result ->
                    val restaurants = result.toObjects(RestaurantDTO::class.java)
                    for(i in restaurants.indices){
                        restaurants[i].id = result.documents[i].id
                    }
                    callback.onSuccess(restaurants[0])
                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error getting documents.", exception)
                    callback.onFailed(exception)
                }
        }
    }