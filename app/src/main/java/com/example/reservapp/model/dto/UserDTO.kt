package com.example.reservapp.model.dto

data class UserDTO(
    val idType:String = "",
    var id:String = "",
    val fullName:String ="",
    val phone:String= "",
    var email: String="",
    val allergies: MutableList<AllergyDTO>? = mutableListOf()
)
