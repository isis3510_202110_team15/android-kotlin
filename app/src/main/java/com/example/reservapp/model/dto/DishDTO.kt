package com.example.reservapp.model.dto

class DishDTO(
    val name:String = "",
    val description:String? = "",
    val price:Double = 0.0,
    val ingredients: MutableList<String>? = null
        )