package com.example.reservapp.model.service

import java.lang.Exception

interface Callback<T> {

    fun onSuccess(result: T?)

    fun onFailed(exception: Exception)
}

interface CallbackAllergies<T, R, S> {

    fun onSuccess(result: T?, result2: R?, result3: R?)

    fun onFailed(exception: Exception)
}
