package com.example.reservapp.model.dto

data class RestaurantDTO(
    var id: String = "",
    val name: String = "",
    val address: String = "",
    val city: String = "",
    val state: String = "",
    val image: String? = "",
    val description: String? = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    var distance: Double? = Double.MAX_VALUE,
    val neighborhood : String = "",
    val categoryName : String = ""
)
