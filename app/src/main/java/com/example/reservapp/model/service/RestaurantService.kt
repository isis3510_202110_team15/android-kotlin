package com.example.reservapp.model.service

import android.util.Log
import com.example.reservapp.model.dto.RestaurantDTO
import com.example.reservapp.presenter.RestaurantsPresenter
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class RestaurantService(private val presenter: RestaurantsPresenter) {

    fun getRestaurantsByCategory(category: String){
        val docRef = Firebase.firestore.collection("restaurants").whereEqualTo("category", category)
        docRef.get().addOnSuccessListener { documentSnapshot ->
            val restaurants = documentSnapshot.toObjects(RestaurantDTO::class.java)
            for (i in restaurants.indices) {
                restaurants[i].id = documentSnapshot.documents[i].id
            }
            Log.i(
                "RestaurantsService",
                "-------------------------->Restaurants ${restaurants}<-----------------------------"
            )
            presenter.manageFirebaseSuccess(restaurants)
        }
    }

    fun bookTable(date: String, restaurantId: String, restaurantName: String, restaurantCategory: String, restaurantAddress: String, image: String, peopleQuantity: Int, user: String){

        val data = hashMapOf(
            "date" to date,
            "restaurantId" to restaurantId,
            "restaurantName" to restaurantName,
            "restaurantCategory" to restaurantCategory,
            "restaurantAddress" to restaurantAddress,
            "image" to image,
            "peopleQuantity" to peopleQuantity,
            "user" to user
        )

        Firebase.firestore.collection("bookings")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.d("AppointmentSuccess", "Booking added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w("AppointmentFailure", "Error adding document", e)
            }
    }
}