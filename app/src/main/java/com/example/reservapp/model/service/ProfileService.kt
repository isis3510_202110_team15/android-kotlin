package com.example.reservapp.model.service

import android.content.ContentValues.TAG
import android.util.Log
import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.UserDTO
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class ProfileService {

    fun obtainUserAndAllergies(email: String, callback: CallbackAllergies<UserDTO, MutableList<AllergyDTO>, MutableList<AllergyDTO>>) {

        val db = Firebase.firestore

        val collRef = db.collection("users").document(email)
        collRef
            .get()
            .addOnSuccessListener { document ->
                if(document != null){

                    val usuario = document.toObject(UserDTO::class.java)
                    val profileAllergies = usuario?.allergies
                    if (usuario != null) {
                        usuario.email = document.id
                    }

                    db.collection("allergies")
                        .get()
                        .addOnSuccessListener { result ->
                            val allergies = result.toObjects(AllergyDTO::class.java)

                            for(i in allergies.indices){
                                allergies[i].id = result.documents[i].id
                            }
                            callback.onSuccess(usuario, allergies, profileAllergies)

                        }

                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
                callback.onFailed(exception)
            }
    }

    fun putAllergies(email: String, allergies: MutableList<AllergyDTO>, callback: Callback<String>){
        val db = Firebase.firestore

        val collRef = db.collection("users").document(email)

        db.runTransaction{
            val updates = hashMapOf<String, Any>(
                "allergies" to FieldValue.delete()
            )

            collRef.update(updates).addOnCompleteListener{
                collRef
                    .update("allergies", allergies)
                    .addOnSuccessListener {
                        Log.d(TAG, "Allergies successfully updated!")
                        callback.onSuccess("The allergies update was successful")
                    }
                    .addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }
            }

        }


    }

}