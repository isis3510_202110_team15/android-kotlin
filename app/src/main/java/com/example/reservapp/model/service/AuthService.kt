package com.example.reservapp.model.service

import android.util.Log
import com.example.reservapp.model.dto.UserDTO
import com.example.reservapp.presenter.FirebaseActions
import com.example.reservapp.presenter.AuthPresenter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class AuthService(private val presenter: AuthPresenter) {

    fun registerUserEmail(email: String, password: String,idType:String,id:String,fullName:String,phone:String) {
        FirebaseAuth.getInstance()
            .createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.i(
                        "AuthService",
                        "------------------------------------>Email register successful<------------------------------------"
                    )
                    registerUserData(idType, id, fullName, phone, email)
                } else {
                    Log.e(
                        "AuthService",
                        "------------------------------------>Email register failed: ${task.exception?.message}<------------------------------------"
                    )
                    presenter.manageFirebaseError("registerFailed")
                }
            }
    }

    private fun registerUserData(idType:String,id:String,fullName:String,phone:String,email: String){
        val user = UserDTO(idType, id, fullName, phone, email, mutableListOf())
        Firebase.firestore.collection("users").document(email).set(user).addOnCompleteListener{ task ->
            if (task.isSuccessful) {
                Log.i(
                    "AuthService",
                    "------------------------------------>Sign Up successful<------------------------------------"
                )
                presenter.manageFirebaseSuccess(FirebaseActions.SIGN_UP)
            } else {
                task.exception
                Log.e(
                    "AuthService",
                    "------------------------------------>Sign Up failed: ${task.exception?.message}<------------------------------------"
                )
                presenter.manageFirebaseError("logInFailed")
            }
        }
    }

    fun login(email: String, password: String) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.i(
                        "AuthService",
                        "------------------------------------>Log in successful<------------------------------------"
                    )
                    presenter.manageFirebaseSuccess(FirebaseActions.LOGIN)
                } else {
                    task.exception
                    Log.e(
                        "AuthService",
                        "------------------------------------>Log in failed: ${task.exception?.message}<------------------------------------"
                    )
                    presenter.manageFirebaseError("logInFailed")
                }
            }
    }
}
