package com.example.reservapp.model.service

import android.util.Log
import com.example.reservapp.model.dto.*
import com.example.reservapp.presenter.MenusPresenter
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class MenuService(private val presenter: MenusPresenter) {

    fun getMenuByRestaurantId(restaurantId: String){
        val docRef = Firebase.firestore.collection("menu").document(restaurantId)
        docRef.get().addOnSuccessListener { documentSnapshot ->
            val menu = documentSnapshot.toObject(MenuDTO::class.java)
            Log.i("RestaurantsService","-------------------------->Restaurants ${menu}<-----------------------------")
            presenter.manageFirebaseSuccess(menu)
        }
    }

    fun getUserAllergies(callback: Callback<MutableList<AllergyDTO>>, userEmail: String){
        val docRef = Firebase.firestore.collection("users").document(userEmail)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val usuario = document.toObject(UserDTO::class.java)
                    val profileAllergies = usuario?.allergies
                    if (profileAllergies != null) {
                        callback.onSuccess(profileAllergies)
                    }
                }
            }
    }

}