package com.example.reservapp.model.dto

data class CategoryDTO(var id:String="", val name:String="",val image:String?="")
