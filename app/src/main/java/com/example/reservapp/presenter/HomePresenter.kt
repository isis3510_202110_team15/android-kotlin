package com.example.reservapp.presenter

import com.example.reservapp.model.dto.CategoryDTO

interface HomePresenter {

    fun manageFirebaseSuccess(result:List<CategoryDTO>?)
}