package com.example.reservapp.presenter

import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.MenuDTO

interface MenusPresenter {

    fun manageFirebaseSuccess(menu:MenuDTO?)
    fun getUserAllergies(user: String)
    fun allergiesSuccess(profileAllergies: MutableList<AllergyDTO>)

}