package com.example.reservapp.presenter

import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.CategoryDTO
import com.example.reservapp.model.dto.UserDTO
import com.example.reservapp.model.service.Callback
import com.example.reservapp.model.service.CallbackAllergies
import com.example.reservapp.model.service.ProfileService
import com.example.reservapp.view.ProfileView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class ProfilePresenterImp (private val view: ProfileView): ProfilePresenter{

    private val profileService: ProfileService = ProfileService()

    suspend fun obtainUserAndAllergies(email: String){

        return withContext(Dispatchers.IO){
            profileService.obtainUserAndAllergies(email, object :
                CallbackAllergies<UserDTO, MutableList<AllergyDTO>, MutableList<AllergyDTO>> {
                override fun onSuccess(
                    result: UserDTO?,
                    result2: MutableList<AllergyDTO>?,
                    result3: MutableList<AllergyDTO>?
                ) {
                    if (result != null) {
                        manageFirebaseSuccess(result, result2 as MutableList<AllergyDTO>,
                            result3 as MutableList<AllergyDTO>
                        )
                    }
                }

                override fun onFailed(exception: Exception) {
                    view.showError("Error consulting allergies")
                }

            })
        }

    }

    suspend fun putAllergies(email: String, allergies: MutableList<AllergyDTO>){

        return withContext(Dispatchers.IO){
            profileService.putAllergies(email, allergies, object : Callback<String>{
                override fun onSuccess(result: String?) {
                    if (result != null) {
                        manageFirebaseSuccessAllergies(result)
                    }
                }

                override fun onFailed(exception: Exception) {
                    manageFirebaseError("Error updating the allergies")
                }

            })
        }
    }

    override fun manageFirebaseError(message: String) {
        view.showError(message)
    }

    override fun manageFirebaseSuccess(userDTO: UserDTO, allergiesList: MutableList<AllergyDTO>, profileAllergies: MutableList<AllergyDTO>) {
        view.setUp(userDTO, allergiesList, profileAllergies)
    }

    override fun manageFirebaseSuccessAllergies(allergies: String) {
        view.confirmUpdateAllergies(allergies)
    }

}