package com.example.reservapp.presenter

import com.example.reservapp.model.dto.BookingDTO
import com.example.reservapp.model.service.BookingsService
import com.example.reservapp.model.service.Callback
import com.example.reservapp.view.BookingsView
import java.lang.Exception

class BookingsPresenter(val view: BookingsView) {

    private val bookingsService: BookingsService = BookingsService()

    fun getBookingsbyUser(user : String) {

        bookingsService.getBookingsByUser(object: Callback<MutableList<BookingDTO>> {
            override fun onSuccess(result: MutableList<BookingDTO>?) {

                view.setUp(result!!)
                view.getBookings(result)
                view.drawBookings(result)
            }

            override fun onFailed(exception: Exception) {

            }

        }, user)
    }
}