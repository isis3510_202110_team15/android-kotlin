package com.example.reservapp.presenter

interface AuthPresenter {

    fun manageFirebaseError(message:String)

    fun manageFirebaseSuccess(actions: FirebaseActions)
}