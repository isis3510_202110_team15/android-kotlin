package com.example.reservapp.presenter

import com.example.reservapp.model.dto.BookingDTO
import com.example.reservapp.model.dto.RestaurantDTO
import com.example.reservapp.model.service.Callback
import com.example.reservapp.model.service.StadisticsService
import com.example.reservapp.view.util.StadisticsView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class StadisticsPresenterImp(val view: StadisticsView) {

    private val stadisticsService : StadisticsService = StadisticsService()

    suspend fun getBookingsbyUser(user : String) {

        return withContext(Dispatchers.IO){
            stadisticsService.getBookingsByUser(object: Callback<MutableList<BookingDTO>> {
                override fun onSuccess(result: MutableList<BookingDTO>?) {
                    view.drawStadistics(result!!)
                }

                override fun onFailed(exception: Exception) {
                    view.showError("Error while geting bookings in Stadistics View")
                }

            }, user)
        }

    }

    suspend fun getResturantByNameAndCategory(name:String, category:String){
        return withContext(Dispatchers.IO){
            stadisticsService.getResturantByNameAndCategory(name, category, object: Callback<RestaurantDTO>{
                override fun onSuccess(result: RestaurantDTO?) {
                    if (result != null) {
                        view.drawNeighborhood(result)
                    }
                    else{
                        view.showError("Error drawing the favorite neighborhood")
                    }
                }

                override fun onFailed(exception: Exception) {
                    view.showError("Error getting the favori    te neighborhood")
                }

            })
        }


    }
}