package com.example.reservapp.presenter

import com.example.reservapp.model.dto.RestaurantDTO

interface RestaurantsPresenter {

    fun manageFirebaseSuccess(restaurantsList:MutableList<RestaurantDTO>)
}