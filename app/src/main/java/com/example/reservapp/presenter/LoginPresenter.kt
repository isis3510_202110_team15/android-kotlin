package com.example.reservapp.presenter

import com.example.reservapp.model.service.AuthService
import com.example.reservapp.view.LoginView

class LoginPresenter(private val view: LoginView):AuthPresenter {

    private val authService: AuthService = AuthService(this)

    fun login(email: String, password: String) {
            authService.login(email, password)
    }

    override fun manageFirebaseError(message:String) {
        view.showError(view.translateError(message))
    }

    override fun manageFirebaseSuccess(actions: FirebaseActions) {
        @Suppress("NON_EXHAUSTIVE_WHEN")
        when(actions){
            FirebaseActions.LOGIN -> view.goToHome(true)
        }
    }
}