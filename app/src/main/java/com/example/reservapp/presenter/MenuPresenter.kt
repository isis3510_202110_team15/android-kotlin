package com.example.reservapp.presenter

import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.MenuDTO
import com.example.reservapp.model.service.Callback
import com.example.reservapp.model.service.MenuService
import com.example.reservapp.view.MenuView
import java.lang.Exception

class MenuPresenter(val view: MenuView) : MenusPresenter {

    private val menuService: MenuService = MenuService(this)

    fun getMenuByRestaurantId(id : String) {
        return menuService.getMenuByRestaurantId(id)
    }

    override fun manageFirebaseSuccess(menu: MenuDTO?) {
        view.drawMenu(menu)
    }

    override fun getUserAllergies(user: String) {

        menuService.getUserAllergies(object: Callback<MutableList<AllergyDTO>> {
            override fun onSuccess(result: MutableList<AllergyDTO>?) {
                view.setUp(result!!)
            }

            override fun onFailed(exception: Exception) {
            }

        }, user)
    }

    override fun allergiesSuccess(profileAllergies: MutableList<AllergyDTO>) {
        view.setAllergies(profileAllergies)
    }
}