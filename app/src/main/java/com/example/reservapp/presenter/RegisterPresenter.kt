package com.example.reservapp.presenter

import com.example.reservapp.model.service.AuthService
import com.example.reservapp.view.RegisterView

class RegisterPresenter(private val view: RegisterView): AuthPresenter {

    private val authService: AuthService = AuthService(this)

    fun register(email:String,password:String,idType:String,id:String,fullName:String,phone:String){
        authService.registerUserEmail(email, password,idType, id, fullName, phone)
    }

    override fun manageFirebaseError(message:String) {
        view.showError(view.translateError(message))
    }

    override fun manageFirebaseSuccess(actions: FirebaseActions) {
        @Suppress("NON_EXHAUSTIVE_WHEN")
        when(actions){
            FirebaseActions.SIGN_UP -> view.goToLogin()
        }
    }
}