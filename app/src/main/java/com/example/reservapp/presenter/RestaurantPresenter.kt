package com.example.reservapp.presenter

import com.example.reservapp.model.dto.RestaurantDTO
import com.example.reservapp.model.service.RestaurantService
import com.example.reservapp.view.RestaurantView

class RestaurantPresenter(val view: RestaurantView):RestaurantsPresenter{

    private val restaurantService: RestaurantService = RestaurantService(this)

    fun getRestaurants(category : String) {
       return restaurantService.getRestaurantsByCategory(category)
    }

    fun bookTable(
        date: String,
        restaurantId: String,
        restaurantName: String,
        restaurantCategory: String,
        restaurantAddress: String,
        image: String,
        peopleQuantity: Int,
        user: String

    ){
        showMessage("Booking was created")
        return restaurantService.bookTable(date, restaurantId, restaurantName, restaurantCategory, restaurantAddress, image, peopleQuantity, user)
    }

    fun showMessage(message:String){
        view.showMessage(message)
    }

    override fun manageFirebaseSuccess(
        restaurantsList: MutableList<RestaurantDTO>
    ) {
        view.drawRestaurant(restaurantsList)
    }
}