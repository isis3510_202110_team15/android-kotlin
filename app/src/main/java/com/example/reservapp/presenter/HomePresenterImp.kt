package com.example.reservapp.presenter

import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.CategoryDTO
import com.example.reservapp.model.dto.RestaurantDTO
import com.example.reservapp.model.service.Callback
import com.example.reservapp.model.service.HomeService
import com.example.reservapp.view.HomeView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class HomePresenterImp(private val view: HomeView): HomePresenter {

    private val homeService: HomeService = HomeService()

    suspend fun getCategories() {
        return withContext(Dispatchers.IO){
            homeService.obtainCategoriesRestaurants(object : Callback<List<CategoryDTO>> {
                override fun onFailed(exception: Exception) {
                    view.showError("Error obtaining the categories")
                }

                override fun onSuccess(result: List<CategoryDTO>?) {
                    view.setUp(result)
                }

            })
        }
    }

    suspend fun getAllergiesUser(email:String) {
        return withContext(Dispatchers.IO){
            homeService.obtainAllergiesUser(email, object:Callback<List<AllergyDTO>>{
                override fun onSuccess(result: List<AllergyDTO>?) {

                    if (result != null) {
                        view.setUpAllergiesInSharedPreferences(result)
                    }
                }

                override fun onFailed(exception: Exception) {
                    //view.showError("Error obtaining the allergies of the user to share")
                }

            })
        }
    }

    fun getRestaurantByQR(qrCode: String){
        homeService.obtainRestaurantByQR(qrCode, object  : Callback<RestaurantDTO>{
            override fun onSuccess(result: RestaurantDTO?) {
                if (result != null) {
                    view.intentQrResult(result)
                }
                else{
                    view.showError("The qr does not belong to any restaurant of our application")
                }
            }

            override fun onFailed(exception: Exception) {
                view.showError("Error obtaining the restaurant via QR")
            }

        })
    }

    override fun manageFirebaseSuccess(result: List<CategoryDTO>?) {
        view.setUp(result)
    }
}