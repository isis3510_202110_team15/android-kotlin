package com.example.reservapp.presenter

import com.example.reservapp.model.dto.AllergyDTO
import com.example.reservapp.model.dto.UserDTO

interface ProfilePresenter {

    fun manageFirebaseError(message:String)

    fun manageFirebaseSuccess(userDTO: UserDTO, allergiesList: MutableList<AllergyDTO>, profileAllergies: MutableList<AllergyDTO>)

    fun manageFirebaseSuccessAllergies(allergies: String)

}